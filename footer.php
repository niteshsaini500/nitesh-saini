
<!-- Start Loading
===================================-->
<!-- <div class="loading">
    <div class="sk-folding-cube">
        <div class="sk-cube1 sk-cube"></div>
        <div class="sk-cube2 sk-cube"></div>
        <div class="sk-cube4 sk-cube"></div>
        <div class="sk-cube3 sk-cube"></div>
    </div>
</div> -->
<!-- End Loading ==================-->

<!-- start the script -->
<script src="js/jquery-2.2.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.countTo.min.js"></script>
<script src="js/jquery.nicescroll.min.js"></script>
<script src="js/typed.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/progressbar.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/ajax.js"></script>

<!-- end the script -->
</body>
</html>
